package com.androidegitim.dovizkurlari.constant;

/**
 * Created by Arda Kaplan on 6.04.2018 - 12:32
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */
public final class SharedPreferencesKeyConstants {

    private SharedPreferencesKeyConstants() {

    }

    public static final String OPENING_COUNT = "OPENING_COUNT";
}
