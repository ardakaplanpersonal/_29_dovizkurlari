package com.androidegitim.dovizkurlari.constant;

/**
 * Created by Arda Kaplan on 26.02.2018 - 19:08
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class SettingsForApplicationConstant {

    private SettingsForApplicationConstant() {

    }

    public static final boolean ENABLE_LOGGING = false;

    public static final boolean GET_DATA_FROM_LOCAL = false;
}
