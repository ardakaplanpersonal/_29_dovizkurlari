package com.androidegitim.dovizkurlari.constant;

/**
 * Created by Arda Kaplan on 26.02.2018 - 19:45
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class PassingDataKeyConstants {

    private PassingDataKeyConstants() {

    }

    public static final String CURRENCIES = "CURRENCIES";

    public static final String DAILY_VALUES = "DAILY_VALUES";

    public static final String CURRENY_CODE="CURRENCY_CODE";
}
