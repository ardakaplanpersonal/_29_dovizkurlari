package com.androidegitim.dovizkurlari;

import android.app.Application;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.androidegitimlibrary.helpers.SharedPreferencesHelpers;
import com.androidegitim.dovizkurlari.constant.PassingDataKeyConstants;
import com.androidegitim.dovizkurlari.constant.SettingsForApplicationConstant;
import com.androidegitim.dovizkurlari.constant.SharedPreferencesKeyConstants;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Arda Kaplan on 26.02.2018 - 19:07
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class DovizKurlariApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        calculateOpeningCount();

        Fabric.with(this, new Crashlytics());

        RDALogger.start(getString(R.string.app_name)).enableLogging(SettingsForApplicationConstant.ENABLE_LOGGING);
    }

    private void calculateOpeningCount() {

        int openingCount = SharedPreferencesHelpers.getInt(this, SharedPreferencesKeyConstants.OPENING_COUNT);

        openingCount = openingCount + 1;

        SharedPreferencesHelpers.putInt(this, SharedPreferencesKeyConstants.OPENING_COUNT, openingCount);

        RDALogger.info("AÇILMA SAYISI " + openingCount);
    }
}
