package com.androidegitim.dovizkurlari.ui;

import com.androidegitim.dovizkurlari.be.models.Currency;
import com.androidegitim.dovizkurlari.helpers.DateFormatter;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by philipp on 02/06/16.
 */
public class XAxisValuesFormatter implements IAxisValueFormatter {

    private ArrayList<Currency> currencyDailyValues;

    public XAxisValuesFormatter(ArrayList<Currency> currencyDailyValues) {
        this.currencyDailyValues = currencyDailyValues;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        long timestamp = currencyDailyValues.get((int) value).getUpdateDate();

        return DateFormatter.getHHmm(timestamp * 1000);
    }
}
