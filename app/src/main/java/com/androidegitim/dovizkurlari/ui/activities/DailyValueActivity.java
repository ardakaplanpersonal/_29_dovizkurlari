package com.androidegitim.dovizkurlari.ui.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.dovizkurlari.R;
import com.androidegitim.dovizkurlari.be.models.Currency;
import com.androidegitim.dovizkurlari.constant.PassingDataKeyConstants;
import com.androidegitim.dovizkurlari.ui.XAxisValuesFormatter;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;

/**
 * Created by Arda Kaplan on 11.03.2018 - 18:48
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

@SuppressWarnings("unchecked")
public class DailyValueActivity extends BaseActivity {

    private int defaultValueCount = 15;

    @BindView(R.id.daily_value_barchart)
    BarChart barChart;
    @BindView(R.id.daily_value_textview_selected_item_hour)
    TextView selectedItemHourTextView;
    @BindView(R.id.daily_value_textview_selected_item_value)
    TextView selectedItemValueText;
    @BindView(R.id.daily_value_textview_header)
    TextView headerTextView;
    @BindView(R.id.daily_value_seekbar_value_count)
    SeekBar currencyValueCountSeekBar;
    @BindView(R.id.daily_value_textview_value_count)
    TextView valueCountTextView;

    private ArrayList<Currency> currencyDailyValues;

    @SuppressWarnings("ConstantConditions")
    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_daily_value);

        currencyDailyValues = (ArrayList<Currency>) getIntent().getExtras().getSerializable(PassingDataKeyConstants.DAILY_VALUES);

        if (currencyDailyValues.size() < defaultValueCount) {

            defaultValueCount = currencyDailyValues.size();
        }

        RDALogger.info(currencyDailyValues.size());

        for (int i = 0; i < currencyDailyValues.size(); i++) {


            RDALogger.info(currencyDailyValues.get(i));
        }


        headerTextView.setText(String.format("%s günü\n%s satış değerleri", formatDate("dd/MM/yyyy", currencyDailyValues.get(0).getUpdateDate()), getIntent().getExtras().getString(PassingDataKeyConstants.CURRENY_CODE)));

        adjustChart();

        adjustSeekbar();

        setData(defaultValueCount);

        valueCountTextView.setText(String.valueOf(defaultValueCount));
    }

    private void adjustSeekbar() {

        currencyValueCountSeekBar.setMax(currencyDailyValues.size());

        currencyValueCountSeekBar.setProgress(defaultValueCount);

        currencyValueCountSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                valueCountTextView.setText(String.valueOf(progress));

                setData(progress);

                barChart.invalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void adjustChart() {

        barChart.setDrawBarShadow(false);

        barChart.setDrawValueAboveBar(true);

        barChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(15);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(true);

        barChart.setDrawGridBackground(true);

        barChart.setDrawValueAboveBar(true);

        IAxisValueFormatter xAxisFormatter = new XAxisValuesFormatter(currencyDailyValues);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(8);
        xAxis.setValueFormatter(xAxisFormatter);


        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(5, true);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(getMaximumValue() + 1);
        leftAxis.setAxisMinimum((getMinimumValue() - 1));

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setLabelCount(5, true);
        rightAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        rightAxis.setSpaceTop(getMaximumValue() + 1);
        rightAxis.setAxisMinimum((getMinimumValue() - 1));


        Legend legend = barChart.getLegend();
        legend.setForm(Legend.LegendForm.NONE);

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry entry, Highlight h) {

                selectedItemHourTextView.setText(formatDate("hh:mm:ss", currencyDailyValues.get((int) entry.getX()).getUpdateDate()));

                selectedItemValueText.setText(String.valueOf(entry.getY()));
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    private String formatDate(String format, long date) {

        SimpleDateFormat formatter = new SimpleDateFormat(format, new Locale("tr"));

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(Long.valueOf(date + "000"));

        return formatter.format(calendar.getTime());
    }


    private void setData(int count) {

        ArrayList<BarEntry> chartItems = new ArrayList<>();

        for (int i = 0; i < count; i++) {

            Currency currency = currencyDailyValues.get(i);

            chartItems.add(new BarEntry(i, (float) (currency.getSellingPrice())));
        }

        BarDataSet set1;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {

            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);

            set1.setValues(chartItems);

            barChart.getData().notifyDataChanged();

            barChart.notifyDataSetChanged();

        } else {

            set1 = new BarDataSet(chartItems, currencyDailyValues.get(0).getCode());

            set1.setDrawIcons(false);

            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();

            dataSets.add(set1);

            BarData data = new BarData(dataSets);

            data.setValueTextSize(10f);

            data.setBarWidth(0.9f);

            barChart.setData(data);
        }
    }


    private float getMaximumValue() {

        double max = 0;

        for (int i = 0; i < currencyDailyValues.size(); i++) {

            if (currencyDailyValues.get(i).getSellingPrice() > max) {

                max = currencyDailyValues.get(i).getSellingPrice();
            }
        }

        return (float) max;
    }

    private float getMinimumValue() {

        double min = currencyDailyValues.get(0).getSellingPrice();

        for (int i = 1; i < currencyDailyValues.size(); i++) {

            if (currencyDailyValues.get(i).getSellingPrice() < min) {

                min = currencyDailyValues.get(i).getSellingPrice();
            }
        }

        return (float) min;
    }

}
