package com.androidegitim.dovizkurlari.ui.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.androidegitimlibrary.helpers.SharedPreferencesHelpers;
import com.androidegitim.dovizkurlari.R;
import com.androidegitim.dovizkurlari.be.DovizKurlariRequestListener;
import com.androidegitim.dovizkurlari.be.models.Currency;
import com.androidegitim.dovizkurlari.be.services.CurrencyService;
import com.androidegitim.dovizkurlari.constant.PassingDataKeyConstants;
import com.androidegitim.dovizkurlari.constant.SharedPreferencesKeyConstants;

import java.util.ArrayList;

/**
 * Created by Arda Kaplan on 26.02.2018 - 19:05
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class SplashActivity extends BaseActivity {

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_splash);

        getAllCurrencies();
    }

    private void getAllCurrencies() {

        CurrencyService.getAllCurrencies(new DovizKurlariRequestListener<ArrayList<Currency>>(SplashActivity.this) {

            @Override
            public void onSuccess(ArrayList<Currency> currencyArrayList) {
                super.onSuccess(currencyArrayList);

                passScreen(currencyArrayList);
            }
        });
    }

    private void passScreen(ArrayList<Currency> currencyArrayList) {

        Intent intent = new Intent(this, CurrencyListActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        intent.putExtra(PassingDataKeyConstants.CURRENCIES, currencyArrayList);

        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
//        geri tus calismayacak
    }
}
