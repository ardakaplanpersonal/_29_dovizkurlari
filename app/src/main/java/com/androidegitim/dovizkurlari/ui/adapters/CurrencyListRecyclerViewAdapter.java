package com.androidegitim.dovizkurlari.ui.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidegitim.dovizkurlari.R;
import com.androidegitim.dovizkurlari.be.DovizKurlariRequestListener;
import com.androidegitim.dovizkurlari.be.models.Currency;
import com.androidegitim.dovizkurlari.be.services.CurrencyService;
import com.androidegitim.dovizkurlari.constant.PassingDataKeyConstants;
import com.androidegitim.dovizkurlari.ui.activities.DailyValueActivity;
import com.androidegitim.dovizkurlari.ui.dialogs.ProgressDialog;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arda Kaplan on 26.02.2018 - 20:52
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class CurrencyListRecyclerViewAdapter extends RecyclerView.Adapter<CurrencyListRecyclerViewAdapter.RecyclerViewHolder> {

    private List<Currency> dataList;

    private DecimalFormat decimalFormat = new DecimalFormat("0.0000");

    private Activity activity;

    public CurrencyListRecyclerViewAdapter(Activity activity, List<Currency> dataList) {
        this.dataList = dataList;
        this.activity = activity;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_currency, parent, false);

        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        final Currency currency = dataList.get(position);

        holder.currencyCodeTextView.setText(currency.getCode());
        holder.currencyNameTextView.setText(currency.getFullName());
        holder.currencySellingTextView.setText(decimalFormat.format(currency.getSellingPrice()));
        holder.currenyBuyingTextView.setText(decimalFormat.format(currency.getBuyingPrice()));
        holder.updateDateTextView.setText(adjustDate(currency.getUpdateDate()));

        if (position % 2 == 0) {

            holder.rootLinearLayout.setBackgroundColor(activity.getResources().getColor(R.color.icon_light_grey));

        } else {

            holder.rootLinearLayout.setBackgroundColor(activity.getResources().getColor(android.R.color.white));
        }

        holder.rootLinearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ProgressDialog.open(activity);

                CurrencyService.getDailyValues(currency.getCode(), new DovizKurlariRequestListener<ArrayList<Currency>>(activity) {

                    @Override
                    public void onSuccess(ArrayList<Currency> currencyDailyValues) {
                        super.onSuccess(currencyDailyValues);

                        ProgressDialog.close();

                        Intent intent = new Intent(activity, DailyValueActivity.class);

                        intent.putExtra(PassingDataKeyConstants.DAILY_VALUES, currencyDailyValues);

                        intent.putExtra(PassingDataKeyConstants.CURRENY_CODE, currency.getCode());

                        activity.startActivity(intent);
                    }
                });
            }
        });
    }


    private String adjustDate(long date) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss", new Locale("tr"));

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(Long.valueOf(date + "000"));

        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row_curreny_linearlayout_root)
        LinearLayout rootLinearLayout;
        @BindView(R.id.row_currency_code)
        TextView currencyCodeTextView;
        @BindView(R.id.row_currency_name)
        TextView currencyNameTextView;
        @BindView(R.id.row_currency_buying)
        TextView currenyBuyingTextView;
        @BindView(R.id.row_currency_selling)
        TextView currencySellingTextView;
        @BindView(R.id.row_curreny_update_date)
        TextView updateDateTextView;

        RecyclerViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }

    }
}