package com.androidegitim.dovizkurlari.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by Arda Kaplan on 26.02.2018 - 19:04
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public abstract class BaseActivity extends AppCompatActivity {


    protected void onCreate(@Nullable Bundle savedInstanceState,int layoutID) {
        super.onCreate(savedInstanceState);

        setContentView(layoutID);

        ButterKnife.bind(this);
    }
}
