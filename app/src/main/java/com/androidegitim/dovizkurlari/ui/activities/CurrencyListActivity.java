package com.androidegitim.dovizkurlari.ui.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.androidegitim.androidegitimlibrary.helpers.DialogHelpers;
import com.androidegitim.androidegitimlibrary.helpers.SharedPreferencesHelpers;
import com.androidegitim.dovizkurlari.R;
import com.androidegitim.dovizkurlari.be.models.Currency;
import com.androidegitim.dovizkurlari.constant.PassingDataKeyConstants;
import com.androidegitim.dovizkurlari.constant.SharedPreferencesKeyConstants;
import com.androidegitim.dovizkurlari.ui.adapters.CurrencyListRecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.BindView;

public class CurrencyListActivity extends BaseActivity {

    @BindView(R.id.currency_list_recycler_view)
    RecyclerView currencyRecyclerView;

    private ArrayList<Currency> currencyArrayList;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_currency_list);

        //noinspection unchecked,ConstantConditions
        currencyArrayList = (ArrayList<Currency>) getIntent().getExtras().get(PassingDataKeyConstants.CURRENCIES);

        fillList();

        showGoToPlayStoreDialog();
    }

    /**
     * 10 mod 3 = 1
     * 10 mod 4 = 2
     * 10 mod 5 = 0
     */
    private void showGoToPlayStoreDialog() {

        int openingCount = SharedPreferencesHelpers.getInt(this, SharedPreferencesKeyConstants.OPENING_COUNT);

        if (openingCount % 5 == 0) {

            DialogHelpers.showDialog(this, "Yorum yap!", "Uygulamayı " + openingCount + ". keredir açıyorsunuz. Demekki uygulamayı sevdiniz ve kullanıyorsunuz. Google Play Store 'da yorum yapıp, puan vermek ister misiniz ?",
                                     "Evet, Markete Git", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    }, "Hayır", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    }, null, null);

        }

    }

    private void fillList() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        currencyRecyclerView.setLayoutManager(linearLayoutManager);

        currencyRecyclerView.setAdapter(new CurrencyListRecyclerViewAdapter(CurrencyListActivity.this, currencyArrayList));
    }
}
