package com.androidegitim.dovizkurlari.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.androidegitim.dovizkurlari.R;

/**
 * Created by Arda Kaplan on 11.03.2018 - 18:54
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class ProgressDialog extends Dialog {

    private static ProgressDialog progresss;

    private ProgressDialog(Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_progress);

        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.progress_animation);

        rotation.setRepeatCount(Animation.INFINITE);

        findViewById(R.id.dialog_progress_imageview).startAnimation(rotation);

        setCancelable(false);

        progresss = this;
    }

    public static void open(Activity activity) {

        if (progresss == null) {

            progresss = new ProgressDialog(activity);
        }

        progresss.show();
    }


    public static void close() {

        if (progresss != null) {

            progresss.dismiss();
        }
    }

    @Override
    @Deprecated
    public void show() {
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();

        progresss = null;
    }
}
