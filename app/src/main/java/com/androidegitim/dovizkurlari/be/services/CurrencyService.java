package com.androidegitim.dovizkurlari.be.services;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.dovizkurlari.be.DovizKurlariRequestListener;
import com.androidegitim.dovizkurlari.be.models.Currency;
import com.androidegitim.dovizkurlari.constant.SettingsForApplicationConstant;
import com.androidegitim.dovizkurlari.helpers.LocalDataReader;
import com.androidegitim.httprequestlibrary.RDAResponse;
import com.androidegitim.httprequestlibrary.helpers.JsonHelpers;
import com.androidegitim.httprequestlibrary.helpers.RDARequestHelper;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arda Kaplan on 26.02.2018 - 19:20
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class CurrencyService {

    class URLS {

        private static final String GET_ALL_CURRENCY_URL = "https://www.doviz.com/api/v1/currencies/all/latest";

        private static final String GET_DAILY_VALUES_URL = "https://doviz.com/api/v1/currencies/%s/daily";
    }

    private CurrencyService() {

    }

    public static void getAllCurrencies(final DovizKurlariRequestListener<ArrayList<Currency>> requestListener) {

        if (SettingsForApplicationConstant.GET_DATA_FROM_LOCAL) {

            String data = LocalDataReader.readFromAssets(requestListener.getActivity(), "getAllCurrencies.txt");

            parseComingData(data, requestListener);

        } else {

            RDARequestHelper.makeGetRequest(requestListener.getActivity(), URLS.GET_ALL_CURRENCY_URL, null, new RDAResponse() {

                @Override
                public void onRequestSuccess(String comingJson) {

                    parseComingData(comingJson, requestListener);
                }

                @Override
                public void onRequestFail(Exception exception) {

                }
            });
        }
    }

    public static void getDailyValues(String currencyName, final DovizKurlariRequestListener<ArrayList<Currency>> requestListener) {

        if (SettingsForApplicationConstant.GET_DATA_FROM_LOCAL) {

            String data = LocalDataReader.readFromAssets(requestListener.getActivity(), "getDailyUSDValues.txt");

            parseComingData(data, requestListener);

        } else {

            String URL = String.format(URLS.GET_DAILY_VALUES_URL, currencyName);

            RDARequestHelper.makeGetRequest(requestListener.getActivity(), URL, null, new RDAResponse() {

                @Override
                public void onRequestSuccess(String comingJson) {

                    parseComingData(comingJson, requestListener);
                }

                @Override
                public void onRequestFail(Exception exception) {

                }
            });
        }


    }

    private static void parseComingData(String comingJson, DovizKurlariRequestListener<ArrayList<Currency>> requestListener) {

        Type collectionType = new TypeToken<List<Currency>>() {
        }.getType();

        ArrayList<Currency> currencyArrayList = (ArrayList<Currency>) JsonHelpers.jsonToList(comingJson, collectionType);

        requestListener.onSuccess(currencyArrayList);
    }

}
