package com.androidegitim.dovizkurlari.be.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Arda Kaplan on 26.02.2018 - 19:16
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Currency implements Serializable {

    @SerializedName("selling")
    private double sellingPrice;
    @SerializedName("update_date")
    private long updateDate;
    private int currency;
    @SerializedName("buying")
    private double buyingPrice;
    @SerializedName("change_rate")
    private double changeRate;
    private String name;
    @SerializedName("full_name")
    private String fullName;
    private String code;

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public double getBuyingPrice() {
        return buyingPrice;
    }

    public void setBuyingPrice(double buyingPrice) {
        this.buyingPrice = buyingPrice;
    }

    public double getChangeRate() {
        return changeRate;
    }

    public void setChangeRate(double changeRate) {
        this.changeRate = changeRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "sellingPrice=" + sellingPrice +
                ", updateDate=" + updateDate +
                ", currency=" + currency +
                ", buyingPrice=" + buyingPrice +
                ", changeRate=" + changeRate +
                ", name='" + name + '\'' +
                ", fullName='" + fullName + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
