package com.androidegitim.dovizkurlari.be;

import android.app.Activity;

import com.androidegitim.dovizkurlari.helpers.ErrorHandler;
import com.androidegitim.httprequestlibrary.RDARequestListener;

/**
 * Created by Arda Kaplan on 26.02.2018 - 19:25
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class DovizKurlariRequestListener<W> extends RDARequestListener<W> {


    public DovizKurlariRequestListener(Activity activity) {
        super(activity);
    }

    @Override
    public void onSuccess(W object) {

    }

    @Override
    public void onFail(Exception exception) {

        ErrorHandler.handle(getActivity(), exception);
    }
}
