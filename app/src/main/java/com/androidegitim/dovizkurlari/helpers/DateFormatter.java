package com.androidegitim.dovizkurlari.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Arda Kaplan on 12.03.2018 - 20:20
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class DateFormatter {

    private DateFormatter() {

    }

    public static String getHHmm(long timeStamp){

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", new Locale("tr"));

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(timeStamp);

        return formatter.format(calendar.getTime());
    }
}
