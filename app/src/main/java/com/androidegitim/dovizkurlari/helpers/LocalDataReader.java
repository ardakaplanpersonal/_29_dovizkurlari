package com.androidegitim.dovizkurlari.helpers;

import android.app.Activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Arda Kaplan on 11.03.2018 - 18:04
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class LocalDataReader {

    private LocalDataReader() {

    }

    public static String readFromAssets(Activity activity, String fileName) {

        StringBuilder data =new StringBuilder();

        try {

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(activity.getAssets().open(fileName), "UTF-8"));

            String line;

            while ((line = bufferedReader.readLine()) != null) {

                data.append(line);
            }

            bufferedReader.close();

        } catch (IOException e) {

            e.printStackTrace();
        }

        return data.toString();
    }
}