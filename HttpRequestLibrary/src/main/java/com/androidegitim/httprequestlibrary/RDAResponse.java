package com.androidegitim.httprequestlibrary;

/**
 * Created by ardakaplan on 31/05/16.
 * <p/>
 * www.ardakaplan.com
 * <p/>
 * arda.kaplan09@gmail.com
 */
public interface RDAResponse {

    void onRequestSuccess(String comingJson);

    void onRequestFail(Exception exception);

}
